package game;
 
import java.util.Random;

// <GameIdentifier>
//<UniqueGameID>THISISARANDOMSTRING<


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement(name="GameIdentifier")
@XmlAccessorType(XmlAccessType.NONE)
public class UniqueGameIdentifier {
 
    @XmlElement(name="UniqueGameID")
    private final String uniqueID;
    
    protected String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 9) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String uniqueID = salt.toString();
        return uniqueID;

    }
   
    public UniqueGameIdentifier() {
    	
        this.uniqueID = this.getRandomString(); // TODO: genereate random string......(e.g. UUID v4)
    }
   
    public UniqueGameIdentifier(String id) {
        this.uniqueID = id;
    }
    
    
   
    public String getUniqueID() {
        return this.uniqueID;
    }
}