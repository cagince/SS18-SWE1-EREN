package game;

@XmlRootElement(name = "ResponseEnvelope")
@XmlAccessorType(XmlAccessType.NONE)
@XmlSeeAlso({UniquePlayerIdentifier.class})
public class ResponseEnvelope<T> {

	
	@XmlElement
	private String exceptionName;
	
	@XmlElement
	private String exceptionMessage;
	
	@XmlElement
	private RequestState state; //ok oder error
	
	@XmlElement
	private T data; 
	
	public ResponseEnvelope(T data) {
	
		state = RequestState.OK;
		this.data = data;
		
		this.exceptionMessage = "default error";
		this.exceptionName = "default error";
		
	}
	
	public ResponseEnvelope(String exceptonName, String exceptionMessage) {
	state = RequestState.ERROR;
	this.data = null;
	
	this.exceptionMessage = exceptionMessage;
	this.exceptionName = exceptionName;
	
	}
	
	public Optional<T> getData() {
		return Optional<T>.ofNullable(data);
	}
	
	public static enum RequestState {
		OK, ERROR
	
	}
	
}
