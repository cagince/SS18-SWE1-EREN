package game;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PlayerRegistration")
@XmlAccessorType(XmlAccessType.NONE)

public class PlayerRegistration {
	
	@XmlElement
	private String studentFirstName; 
	
	@XmlElement
	private String studentLastName;
	
	@XmlElement
	private String studentID;
	
	public PlayerRegistration() {
		this.studentFirstName = "";
		this.studentLastName = "";
		this.studentID = "";
		
	}
	
	public PlayerRegistration(String studentFirstName, String studentLastName, String studentID) {
		super();
		this.studentFirstName = studentFirstName;
		this.studentLastName = studentLastName;
		this.studentID = studentID;
	}

	public String getStudentFirstName() {
		return this.studentFirstName;
	}
	
	public String getStudentLastName() {
		return this.studentLastName;
	}
	
	
	public String getStudentID() {
		return this.studentID;
	}
}