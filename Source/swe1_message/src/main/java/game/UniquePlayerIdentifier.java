package game;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "uniqiePlayerIdentifier")
@XmlAccessorType(XmlAccessType.NONE)

public class UniquePlayerIdentifier {
@XmlElement
private String uniquePlayerID;

public UniquePlayerIdentifier() {
	// TODO: uniqie ID here (zb : uuid4)
	this.uniquePlayerID = "THISISRANDOMIDFORPLAYER"; 
}

public UniquiePlayerIdentifier(String playerId) {
	this.uniquePlayerID = playerId;
	
}

public String getUniquePlayerID() {
	return this.uniquePlayerID;
	
}

}