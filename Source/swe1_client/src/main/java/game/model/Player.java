/**
 * 
 */
package game.model;

/**
 * @author Eren
 *
 */
public class Player {
	
	
	private String name;
	private String surname;
	private String matrNr;
	public Player(String name, String surname, String matrNr) {
		super();
		this.name = name;
		this.surname = surname;
		this.matrNr = matrNr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getMatrNr() {
		return matrNr;
	}
	public void setMatrNr(String matrNr) {
		this.matrNr = matrNr;
	}
	
	
	

}
