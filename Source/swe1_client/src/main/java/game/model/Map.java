/**
 * 
 */
package game.model;

/**
 * @author Eren
 *
 */
public class Map {

	
	private String[][] halfmap1;
	private String[][] halfmap2;
	private boolean[] isValid;// 1: halfmap1 2: halfmap2
	private String[] territoryTypes; // ["mountain", "water", "grass",]
	
	private int[] treasure1position;// 0: x , 1: y, 2: is treasure taken 
	private int[] treasure2position;// 0: x , 1: y, 2:
	// [4, 2, 0]
	private int[] player1position;// 0: x , 1: y
	private int[] player2position;// 0: x , 1: y
	
	
	String getFieldType(int x, int y, boolean isHalmap1)  {

		if(isHalmap1) {
			return this.halfmap1[x][y];
		} else {
			return this.halfmap2[x][y];
		}
	}
	
	boolean isTreasureTaken(boolean isPlayerOne) { 
		// 1 = taken
		// 0 = not taken
		if(isPlayerOne) { // check if player 1
			return this.treasure1position[2] == 1;
		} else {
			return this.treasure2position[2] == 1;
		}
	}
	
	public int[] getPlayer1position() {
		return player1position;
	}



	public void setPlayer1position(int[] player1position) {
		this.player1position = player1position;
	}



	public int[] getPlayer2position() {
		return player2position;
	}



	public void setPlayer2position(int[] player2position) {
		this.player2position = player2position;
	}

	private final int rows = 8;
	private final int columns= 8;
	
	public Map() {
		this.halfmap1 = new String[rows/2][columns];
		this.halfmap2 = new String[rows/2][columns];
		this.isValid = new boolean[2];
	}

	
	
	// Getters And Setters
	public String[][] getHalfmap1() {
		return halfmap1;
	}

	public void setHalfmap1(String[][] halfmap1) {
		this.halfmap1 = halfmap1;
	}

	public String[][] getHalfmap2() {
		return halfmap2;
	}

	public void setHalfmap2(String[][] halfmap2) {
		this.halfmap2 = halfmap2;
	}

	public boolean[] getIsValid() {
		return isValid;
	}

	public void setIsValid(boolean[] isValid) {
		this.isValid = isValid;
	}
	
	
	
	
	
	
	
}
