package game.controller;

import game.view.GameView;

public class GameController {
	
	private MapController mapController;
	private PlayerController playerController;
	private GameView gameView;
	
	
	private final int maxRoundCount = 200;
	
	private int round;
	
	public GameController(MapController mapController, PlayerController playerController) {
		super();
		this.mapController = mapController;
		this.playerController = playerController;
		this.gameView = new GameView();
	}
	public MapController getMapController() {
		return mapController;
	}
	public void setMapController(MapController mapController) {
		this.mapController = mapController;
	}
	public PlayerController getPlayerController() {
		return playerController;
	}
	public void setPlayerController(PlayerController playerController) {
		this.playerController = playerController;
	}
	
	
	void init() {
		
	}
	
	void startGame() {
		
	}
	
	void nextRound() {
		
	}
	
	boolean didFinish() {
		return false;
	}
	
	void notifyWin() {
		
	}
	
	
	
	
	
	
	
	
	
	

}
