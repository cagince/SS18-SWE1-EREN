package game.controller;

import game.model.Map;
import game.view.MapView;

public class MapController {
	
	private Map map;
	private MapView mapView;
	
	public MapController(Map map, MapView mapView) {
		this.map = map;
		this.mapView = mapView;
	}
	public Map getMap() {
		return map;
	}
	public void setMap(Map map) {
		this.map = map;
	}
	public MapView getMapView() {
		return mapView;
	}
	public void setMapView(MapView mapView) {
		this.mapView = mapView;
	}
	
	
	// Generates a map and initializes it to this.map
	void generateNewMap() {
		
	}
	
	
	void setWaterFields() {
		
	}
	
	void setMountainFields() {
		
	}
	
	void setGrasFields() {
	
	}
	
	void toggleMap() {
		
	}
	
	boolean isMapValid(Map map) {
		return false;
	}
	
	void initializeMapForStart() {
		this.setTreasureField();
		this.setPlayerField();
	}
	
	
	void setTreasureField() {
		
	}
	
	void setPlayerField() {
		
	}
	
	void movePlayer(int x , int y) {
		
	}
	
	boolean isMoveValid(int[] move) { // 0: x , 1: y
		return false;
	}
	
	
	

	
	

}
