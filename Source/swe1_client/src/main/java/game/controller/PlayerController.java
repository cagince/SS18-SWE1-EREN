package game.controller;

import game.model.Map;
import game.model.Player;

public class PlayerController {

	
	private Player player;
	
	
	public PlayerController(Player player) {
		this.player = player;
	}
	
	
	
	boolean canMove() { return true;}
	
	
	void joinGame() {
		
	}
	
	void makeMove() {
		
	}
	
	Map generateMap() {
		return new Map();
	}
	
	boolean didWin() {
		return true;
	}
	
	Player getPlayer() {
		return this.player;
	}
	
}
