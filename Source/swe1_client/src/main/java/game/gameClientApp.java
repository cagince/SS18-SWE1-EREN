package game;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.springframework.web.client.RestTemplate;

public class gameClientApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO: send GET request to tournament server
		
		//PART 1 ; get an /game/new
		String url = "htttp://swe.wst.univie.ac.at:18235";
		
		RestTemplate restTemplate = new RestTemplate();
		UniqueGameIdentifier gameId = restTemplate.getForObject(url+"/game/new", UniqueGameIdentifier.class);
		
		
		System.out.println("Received Game ID: " + gameId.getUniqueID());
		
		// Part2: Post an /game/{gameI}/register
		
		// DONE needs data :player register
		
		PlayerRegistration playerReg = new PlayerRegistration("Eren", "Duman", "1027932");
				
				JAXBContext context = JAXBContext.newInstance(PlayerRegistration.class); 
				Marshaller m = context.createMarshaller();
				
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				StringWriter sw = new StringWriter();
				m.marshal(playerReg, sw);
				System.out.print("====");
				System.out.println(sw.toString());
		
				ResponseEnvelope<UniquePlayerIdentifier> gameResponse = 
				restTemplate.postForObject(url + "/game/" +  gameId.getUniqueID() + "/register",
						playerReg, ResponseEnvelope.class);
				
				if (gameResponse.getData().isPresent()) {
					System.out.println("Unique PLayer id: " + gameResponse.getData().get().getUniquePlayerID());
				}
				
				//POST
	}

}
